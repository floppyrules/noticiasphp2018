<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plantilla de BOOTSTRAP 3 - David Fraj Blesa</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

  </head>
  <body>
    
    <section class="container">
      
      <header>
        <?php include('includes/encabezado.php'); ?>
      </header>
      <hr>
      <nav>
        <?php include('includes/menu.php'); ?>
      </nav>

      <main class="row">
        <section class="col-md-8">
          <?php include('paginas/noticias.php'); ?>
        </section>
        <nav class="col-md-4">
          <h3>Categorias</h3>
        </nav>
      </main>

      <footer>
          <?php include('includes/pie.php'); ?>
      </footer>

    </section>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>