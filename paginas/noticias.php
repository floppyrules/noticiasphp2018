<h3>Estas en el Listado de Noticias</h3>



<?php
//1.- Conectar a la BBDD
$servidor='localhost';
$usuario='root';
$clave='';
$base='noticiasphp';
$conexion=new Mysqli($servidor, $usuario, $clave, $base);
$conexion->set_charset('utf8');


//2.- Establecer la pregunta que quiero hacerle, preguntamos en SQL
$sql="SELECT * FROM noticias";

//3.- Ejecuto la prgunta o consulta
$consulta=$conexion->query($sql);

//4.- Procesamos los resultados de la consulta
while($registro=$consulta->fetch_array()){
	//echo $registro['tituloNoticia'];
	//echo '<br>';
	?>
	<article>
		<header>
			<h4><strong>
				<?php echo $registro['tituloNoticia']; ?>
			</strong></h4>
		</header>
		<section>
			<img src="images/<?php echo $registro['imagenNoticia'];?>" class="img-responsive img-rounded" style="float:left; margin:10px; width:200px;">
			<?php echo $registro['textoNoticia']; ?>	
		</section>
		<footer class="text-right" style="clear:both;"><?php echo $registro['fechaNoticia']; ?></footer>
	</article>
	<hr>

	<?php
	
}


//5.- Desconectar de la BBDD
$conexion->close();




?>